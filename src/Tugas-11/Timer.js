import React from 'react';

class Timer extends React.Component{
    constructor(props){
        super(props)
        this.state={
            time: new Date ()
        }
    }
    currentTime(){
        this.setState({
            time:new Date()
        })
    }
    componentDidMount(){
   setInterval(()=> this.currentTime(),1000)
  }


    render(){
        return(
            <>
            <h1 style={{textAlign: "center"}}>
          Sekarang Jam {this.state.time.toLocaleTimeString("en-US")} Hitung Mundur:{}
        </h1>
            </>
        )
    }
}
export default Timer




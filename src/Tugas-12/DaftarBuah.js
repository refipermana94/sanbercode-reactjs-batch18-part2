import React, { Component } from 'react'

class DaftarBuah extends Component {
  constructor(props){
    super(props)
    this.state ={
     dataHargaBuah : [
      {nama: "Semangka", harga: 10000, berat: 1000},
      {nama: "Anggur", harga: 40000, berat: 500},
      {nama: "Strawberry", harga: 30000, berat: 400},
      {nama: "Jeruk", harga: 30000, berat: 1000},
      {nama: "Mangga", harga: 30000, berat: 500}, 
    ] ,
     inputNama : "" ,
     inputHarga : "" ,
     inputBerat : "" ,
     index : -1    
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
      this.handleEdit = this.handleEdit.bind(this)
  }

  handleSubmit(x){
    x.preventDefault();
    let index = this.state.index
    let nama = this.state.inputNama
    let harga = this.state.inputHarga
    let berat = this.state.inputBerat
    let dataHargaBuah = this.state.dataHargaBuah
    let tambahBuah =  dataHargaBuah.push({
      nama : nama,
      harga : harga,
      berat : berat
    })
    if(index === -1){
      this.setState( {tambahBuah})
    }else{
      dataHargaBuah[index]({
        nama : "",
        harga : "",
        berat : ""
      })
       this.setState( {tambahBuah})
    }    
  }
  handleChange = (x) =>{
    var value = x.target.value
    this.setState({
    inputNama: value,
    }); 
     this.setState({
    inputHarga: value,
    }); 
    this.setState({
    inputBerat: value,
    });
  }
 handleDelete = (event) =>{
    let index = event.target.value;
    this.state.dataHargaBuah.splice(index, 1)
    this.setState({dataHargaBuah: this.state.dataHargaBuah})
}
handleEdit(event){
     let index = event.target.value;
     this.setState({inputNama: this.state.dataHargaBuah[index], index})
      this.setState({inputHarga: this.state.dataHargaBuah[index], index})
       this.setState({inputBerat: this.state.dataHargaBuah[index], index})
  };

render(){
return (
      <>
      
        <h1 style={{textAlign : "center"}}> Daftar Harga Buah</h1>
        <table style={{border: "1px solid", width: "40%", margin: "0 auto"}}>
          <thead style={{background: "#aaa"}}>
            <tr>
              <th>Nama</th>
              <th>Harga</th>
              <th>Berat</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody style={{background: "coral"}}>
            {this.state.dataHargaBuah.map((el, index)=> {
              return (
                <>
                  <tr>
                    <td>{el.nama}</td>
                    <td>{el.harga}</td>
                    <td>{el.berat/1000}kg</td>
                    <td><button value={index} onClick={this.handleEdit}>Edit</button>
                        <button style={{marginLeft: "1em"}}value={index} onClick={this.handleDelete}>Delete</button>
                    </td>
                  </tr>
                </>
                )
              })}
          </tbody>
        </table>
        <form style={{textAlign:"center"}} onSubmit={this.handleSubmit}>
          <label>
            Masukkan nama:
          </label>          
          <input type="text"  requerite onChange={this.handleChange} value={this.state.nama}/> <br/>
          <label>
            Masukkan Harga:
          </label>          
          <input type="text" requerite onChange={this.handleChange} value={this.state.harga}/><br/>
          <label>
            Masukkan Berat:
          </label>          
          <input type="text" requerite onChange={this.handleChange} value={this.state.berat}/><br/>
          <input type="submit" value="Submit" />
        </form>
      </>
    )
  }    
}

export default DaftarBuah

import React from "react";
import "./Routes.css"
//import "./style.css"

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Tugas9 from "../Tugas-9/formBeli"
import Tugas10 from "../Tugas-10/tabel"
import Tugas11 from "../Tugas-11/Timer"
import Tugas12 from "../Tugas-12/DaftarBuah"
import Tugas13 from "../Tugas-13/DaftarBuah"
import Tugas14 from "../Tugas-14/DaftarBuah"
// import Toogle from"../Toogle/Toogle"
import "./Routes.css"

const Routes = () => {
  return (
    <Router>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/">Tugas 9</Link>
            </li>
            <li>
              <Link to="/Tugas-10">Tugas10</Link>
            </li>
            <li>
              <Link to="/Tugas-11">Tugas11</Link>
            </li>
            <li>
              <Link to="/Tugas-12">Tugas12</Link>
            </li>
            <li>
              <Link to="/Tugas-13">Tugas13</Link>
            </li>
             <li>
              <Link to="/Tugas-14">Tugas14</Link>
            </li>
            <li style={{float:"right"}}>
                <label class="switch">
                  <input type="checkbox"/>
                  <span class="slider round"></span>
                  </label>
            </li>
            
          </ul>
           <Link to="/" >
                  
                </Link>
        </nav>
        <Switch>
          <Route exact path="/"><Tugas9 /></Route>
          <Route exact path="/Tugas-10" component={Tugas10}></Route>
          <Route exact path="/Tugas-11" component={Tugas11}></Route>
          <Route exact path="/Tugas-12" component={Tugas12}></Route>
          <Route exact path="/Tugas-13" component={Tugas13}></Route>
          <Route exact path="/Tugas-14" component={Tugas14}></Route>
          {/* <Route exact path="/Toogle" component={Toogle}></Route> */}
          

        </Switch>
      </div>
    </Router>
  );
}


export default Routes